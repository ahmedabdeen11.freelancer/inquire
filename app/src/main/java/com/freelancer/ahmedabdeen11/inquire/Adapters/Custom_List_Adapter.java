package com.freelancer.ahmedabdeen11.inquire.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.freelancer.ahmedabdeen11.inquire.Model.Item;
import com.freelancer.ahmedabdeen11.inquire.R;

import java.util.ArrayList;

/**
 * Created by A7MED-Freelancer on 14-May-17.
 */

public class Custom_List_Adapter extends ArrayAdapter<Item> {
    Context c;

    ArrayList<Item> obj;

    public Custom_List_Adapter(Context c, ArrayList<Item> items) {
        super(c, R.layout.custom_list_item, R.id.tv, items);
        this.c = c;
        this.obj = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        Custom_List_Adapter.MyViewHolder myViewHolder = null;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.custom_list_item, parent, false);

            myViewHolder = new Custom_List_Adapter.MyViewHolder(row);
            row.setTag(myViewHolder);

        } else {
            myViewHolder = (Custom_List_Adapter.MyViewHolder) row.getTag();
        }


        myViewHolder.mytv.setText(obj.get(position).getValue());


        return row;
    }

    class MyViewHolder {
        TextView mytv;

        MyViewHolder(View v) {
            mytv = (TextView) v.findViewById(R.id.tv);
        }
    }
}
