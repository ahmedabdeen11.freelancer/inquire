package com.freelancer.ahmedabdeen11.inquire.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.freelancer.ahmedabdeen11.inquire.R;

public class Animated_Textviews extends AppCompatActivity {

    TextView tv1;
    TextView tv2;
    TextView tv3;
    TextView tv4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animated__textviews);

        tv1 = (TextView) findViewById(R.id.txt1);
        tv2 = (TextView) findViewById(R.id.txt2);
        tv3 = (TextView) findViewById(R.id.txt3);
        tv4 = (TextView) findViewById(R.id.txt4);

        reset_anim(tv1, 0);
        reset_anim(tv2, 200);
        reset_anim(tv3, 400);
        reset_anim(tv4, 600);
    }

    public void animate(View view) {
        view_animation(1);
    }


    public void view_animation(final int y) {
        final View v;
        switch (y) {
            case 1:
                v = tv1;
                break;
            case 2:
                v = tv2;
                break;
            case 3:
                v = tv3;
                break;
            case 4:
                v = tv4;
                break;
            default:
                return;
        }
        //-------------Animate Textview -----------//
        Animation left_anim = AnimationUtils.loadAnimation(this, R.anim.txtview_left_anim);
        v.startAnimation(left_anim);
        left_anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                switch (y) {
                    case 1:
                        view_animation(2);
                        break;
                    case 2:
                        view_animation(3);
                        break;
                    case 3:
                        view_animation(4);
                        break;
                    case 4:
                        break;
                    default:
                }

                Animation right_anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.txtview_right_anim);
                v.startAnimation(right_anim);
                right_anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        v.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

    public void reset(View view) {
        tv1.setVisibility(View.VISIBLE);
        tv2.setVisibility(View.VISIBLE);
        tv3.setVisibility(View.VISIBLE);
        tv4.setVisibility(View.VISIBLE);

        reset_anim(tv1, 0);
        reset_anim(tv2, 200);
        reset_anim(tv3, 400);
        reset_anim(tv4, 600);
    }


    public void reset_anim(View v, long delay) {

        Animation reset_anim = AnimationUtils.loadAnimation(this, R.anim.txtview_reset_anim);
        reset_anim.setStartOffset(delay);
        v.startAnimation(reset_anim);

    }
}
