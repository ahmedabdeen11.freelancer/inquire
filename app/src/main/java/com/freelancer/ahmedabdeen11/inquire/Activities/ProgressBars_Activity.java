package com.freelancer.ahmedabdeen11.inquire.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.freelancer.ahmedabdeen11.inquire.Adapters.ProgressBars_RecyclerView;
import com.freelancer.ahmedabdeen11.inquire.R;

import java.util.ArrayList;

public class ProgressBars_Activity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<Integer[]> progress_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_bars);

        progress_list = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.progress_bars_recycleview);
        adapter = new ProgressBars_RecyclerView(ProgressBars_Activity.this, progress_list);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        createProgressBar(10, 7, 3, 2);
        createProgressBar(10, 8, 3, 10, 2, 6, 2.6, 7.8);
        createProgressBar(2, 1, .5, .8, 2);
        createProgressBar(200, 90, 70, 72, 44, 61);
        createProgressBar(2, 1, .5, .8, 2);
    }


    public void createProgressBar(double... data) {

        Integer[] myarr = new Integer[data.length];
        for (int i = 1; i < data.length; i++) {

            double prog = data[i] / data[0];
            prog = prog * 100;
            myarr[i - 1] = (int) prog;
        }

        progress_list.add(myarr);
        adapter.notifyDataSetChanged();

    }


}
