package com.freelancer.ahmedabdeen11.inquire.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.freelancer.ahmedabdeen11.inquire.R;

import java.util.ArrayList;

/**
 * Created by A7MED-Freelancer on 29-May-17.
 */

public class Adapter_test_Gridview extends ArrayAdapter<Integer> {
    Context c;

    ArrayList<Integer> obj;

    public Adapter_test_Gridview(Context c, ArrayList<Integer> items) {
        super(c, R.layout.locked_images_item, R.id.tv, items);
        this.c = c;
        this.obj = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        Adapter_test_Gridview.MyViewHolder myViewHolder = null;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.locked_images_item, parent, false);

            myViewHolder = new Adapter_test_Gridview.MyViewHolder(row);
            row.setTag(myViewHolder);

        } else {
            myViewHolder = (Adapter_test_Gridview.MyViewHolder) row.getTag();
        }


        Glide.with(c).load(obj.get(position)).into(myViewHolder.myImg);
        myViewHolder.lock.setVisibility(View.GONE);
        myViewHolder.blur.setVisibility(View.GONE);


        return row;
    }

    class MyViewHolder {
        ImageView myImg;
        ImageView lock;
        View blur;

        MyViewHolder(View v) {
            myImg = (ImageView) v.findViewById(R.id.recycle_image);
            lock = (ImageView) v.findViewById(R.id.lock);
            blur = v.findViewById(R.id.blur);
        }
    }
}
