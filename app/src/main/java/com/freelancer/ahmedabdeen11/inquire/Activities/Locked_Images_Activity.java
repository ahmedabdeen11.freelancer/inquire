package com.freelancer.ahmedabdeen11.inquire.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.freelancer.ahmedabdeen11.inquire.Adapters.LockedImages_RecyclerView;
import com.freelancer.ahmedabdeen11.inquire.R;

import java.util.ArrayList;

public class Locked_Images_Activity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<Integer> imgs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locked__images);

        //------------ Adding Images data----------//

        imgs = new ArrayList<>();
        imgs.add(R.drawable.img1);
        imgs.add(R.drawable.img2);
        imgs.add(R.drawable.img3);
        imgs.add(R.drawable.img4);
        imgs.add(R.drawable.img5);
        imgs.add(R.drawable.img6);
        imgs.add(R.drawable.img7);
        imgs.add(R.drawable.img8);
        imgs.add(R.drawable.img9);
        imgs.add(R.drawable.img10);
        imgs.add(R.drawable.img1);
        imgs.add(R.drawable.img2);
        imgs.add(R.drawable.img3);
        imgs.add(R.drawable.img4);
        imgs.add(R.drawable.img5);
        imgs.add(R.drawable.img6);
        imgs.add(R.drawable.img7);
        imgs.add(R.drawable.img8);
        imgs.add(R.drawable.img9);
        imgs.add(R.drawable.img10);

        setAvailableImages(5);


    }


    public void setAvailableImages(int availableImageCount) {

        //--------------Setting the RecyclerView ------------//

        recyclerView = (RecyclerView) findViewById(R.id.locked_images_recycle);
        adapter = new LockedImages_RecyclerView(Locked_Images_Activity.this, imgs, availableImageCount);
        layoutManager = new GridLayoutManager(this, 2);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

    }

}
