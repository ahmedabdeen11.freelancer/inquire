package com.freelancer.ahmedabdeen11.inquire.Model;

/**
 * Created by A7MED-Freelancer on 20-May-17.
 */

public class Contact {

    String name;
    int image;

    public Contact() {
    }

    public Contact(String name, int image) {
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
