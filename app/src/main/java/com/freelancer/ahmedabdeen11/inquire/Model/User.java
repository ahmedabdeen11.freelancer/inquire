package com.freelancer.ahmedabdeen11.inquire.Model;

/**
 * Created by A7MED-Freelancer on 11-May-17.
 */

public class User {
    String id;
    String name;
    String email;
    String photourl;

    public User() {
    }

    public User(String id, String name, String email, String photourl) {
        this.name = name;
        this.email = email;
        this.photourl = photourl;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhotourl() {
        return photourl;
    }

    public void setPhotourl(String photourl) {
        this.photourl = photourl;
    }
}
