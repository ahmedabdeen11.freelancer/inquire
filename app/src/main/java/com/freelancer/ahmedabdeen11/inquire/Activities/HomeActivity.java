package com.freelancer.ahmedabdeen11.inquire.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.facebook.login.LoginManager;
import com.freelancer.ahmedabdeen11.inquire.R;
import com.freelancer.ahmedabdeen11.inquire.Services.Datachange_Service;
import com.google.firebase.auth.FirebaseAuth;

public class HomeActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mAuth = FirebaseAuth.getInstance();

    }

    public void goTabsActivity(View view) {
        startActivity(new Intent(getApplicationContext(), TabsActivity.class));
    }

    public void customListActivity(View view) {
        startActivity(new Intent(getApplicationContext(), Custom_List_Activity.class));
    }

    public void profileActivity(View view) {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {

            stopService(new Intent(getApplicationContext(), Datachange_Service.class));
            mAuth.signOut();
            LoginManager.getInstance().logOut();
            Intent intent = new Intent(getApplicationContext(), Login_Activity.class);
            startActivity(intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void goBubblesActivity(View view) {
        startActivity(new Intent(getApplicationContext(), Bubbles_Activity.class));
    }

    public void goLockedImagesActivity(View view) {
        startActivity(new Intent(getApplicationContext(), Locked_Images_Activity.class));
    }

    public void goProgressBarsActivity(View view) {
        startActivity(new Intent(getApplicationContext(), ProgressBars_Activity.class));
    }

    public void goCoordinatorLayout(View view) {
        startActivity(new Intent(getApplicationContext(), CoordinatorLayout_Activity.class));
    }

    public void goCustomDialog(View view) {
        CustomDialog dialog = new CustomDialog(this);
        dialog.show();
    }

    public void goAnimatedTextviews(View view) {
        startActivity(new Intent(getApplicationContext(), Animated_Textviews.class));
    }

    public void goShrinkProgressBar(View view) {
        startActivity(new Intent(getApplicationContext(), Shrinking_ProgressBar.class));
    }
}
