package com.freelancer.ahmedabdeen11.inquire.Services;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.freelancer.ahmedabdeen11.inquire.Activities.Custom_List_Activity;
import com.freelancer.ahmedabdeen11.inquire.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by A7MED-Freelancer on 13-May-17.
 */

public class Datachange_Service extends Service {

    ValueEventListener myValueEventListner;
    boolean first_call;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        first_call = true;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference().child("custom_list");


        ListenerForRequestDone();

//        mDatabaseReference.addListenerForSingleValueEvent(myValueEventListner);

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        first_call = true;
        Intent intent = new Intent("RESTART_SERVICE");
        sendBroadcast(intent);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void ListenerForRequestDone() {

        mDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (first_call == true) {
                    first_call = false;
                } else {
                    Log.v("Firebase_Log", "List Updated");
                    sendNotification("List Updated");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    public void sendNotification(String message) {

        Intent intent = new Intent(this, Custom_List_Activity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        Notification.Builder builder = new Notification.Builder(this);
        builder.setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentTitle("Firebase")
                .setContentText(message);

        Notification notification = builder.build();
        notificationManager.notify(0, notification);

        playNotificationSound();

    }

    public void playNotificationSound() {
        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + this.getPackageName() + "/" + R.raw.demonstrative);
            Ringtone r = RingtoneManager.getRingtone(this, alarmSound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
