package com.freelancer.ahmedabdeen11.inquire.Activities;

import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.freelancer.ahmedabdeen11.inquire.Adapters.Recycler_Adapter;
import com.freelancer.ahmedabdeen11.inquire.Adapters.SpacesItemDecoration;
import com.freelancer.ahmedabdeen11.inquire.Model.Contact;
import com.freelancer.ahmedabdeen11.inquire.R;

import java.util.ArrayList;

public class Bubbles_Activity extends AppCompatActivity {


    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<Contact> contacts;
    TextView tx_more;

    ImageView iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bubbles);

        //-----------Adding Contacts Data -------..

        contacts = new ArrayList<>();
        contacts.add(new Contact("John", R.drawable.john));
        contacts.add(new Contact("Sara", R.drawable.sara));
        contacts.add(new Contact("Jordan", 0));
        contacts.add(new Contact("Ahmed", 0));
        contacts.add(new Contact("Smith", 0));
        contacts.add(new Contact("Smith", 0));
        contacts.add(new Contact("Bellamy", R.drawable.bellamy));
        contacts.add(new Contact("Bellamy", R.drawable.bellamy));
        contacts.add(new Contact("Bellamy", R.drawable.bellamy));


        //--------------------Setting Recycler View---------------------//

        int image_width = (int) getResources().getDimension(R.dimen.img_width);
        int spacingInPixels = (int) (image_width * -.3);
        image_width = (int) (image_width * .7);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int recycler_width = (int) (width * .66);
        int number_of_imgs = recycler_width / image_width;

        recyclerView = (RecyclerView) findViewById(R.id.recycler_bubbles);
        adapter = new Recycler_Adapter(getApplicationContext(), contacts, number_of_imgs);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        recyclerView.setAdapter(adapter);

        //-------------Contact > number of images per width -------------//
        if (contacts.size() > number_of_imgs) {
            tx_more = (TextView) findViewById(R.id.more_contacts);
            tx_more.setVisibility(View.VISIBLE);
            tx_more.setText("+ " + (contacts.size() - number_of_imgs) + " more");
        }


    }
}
