package com.freelancer.ahmedabdeen11.inquire.Activities;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.freelancer.ahmedabdeen11.inquire.R;

/**
 * Created by A7MED-Freelancer on 03-Jul-17.
 */

public class CustomDialog extends Dialog {

    TextView word;
    ImageView img;
    TextView next;

    public CustomDialog(@NonNull final Context context) {
        super(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        setContentView(R.layout.custom_dialog);

        word = (TextView) findViewById(R.id.word);
        img = (ImageView) findViewById(R.id.img);
        next = (TextView) findViewById(R.id.nxt);
        word.setText("RED BULL");
        img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.redbull));
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Next Clicked", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
