package com.freelancer.ahmedabdeen11.inquire.Activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.freelancer.ahmedabdeen11.inquire.Fragments.FragmentA;
import com.freelancer.ahmedabdeen11.inquire.Fragments.FragmentB;
import com.freelancer.ahmedabdeen11.inquire.Fragments.FragmentC;
import com.freelancer.ahmedabdeen11.inquire.R;

public class TabsActivity extends AppCompatActivity {

    ViewPager viewPager;
    TabLayout tabLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs);

        viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        viewPager.setAdapter(new MyAdaper(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
    }

    class MyAdaper extends FragmentPagerAdapter {

        public MyAdaper(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = new FragmentA();
                    break;
                case 1:
                    fragment = new FragmentB();
                    break;
                case 2:
                    fragment = new FragmentC();
                    break;
                default:
                    fragment = new FragmentA();
            }

            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            int x = position + 1;
            return "Tab" + x;
        }
    }
}
