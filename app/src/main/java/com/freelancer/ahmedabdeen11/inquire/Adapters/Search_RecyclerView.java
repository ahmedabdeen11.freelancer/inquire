package com.freelancer.ahmedabdeen11.inquire.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freelancer.ahmedabdeen11.inquire.R;

import java.util.ArrayList;

/**
 * Created by A7MED-Freelancer on 29-May-17.
 */

public class Search_RecyclerView extends RecyclerView.Adapter<Search_RecyclerView.RecyclerViewHolder> {

    ArrayList<String> words;
    Context c;


    public Search_RecyclerView(Context c, ArrayList<String> words) {
        this.c = c;
        this.words = words;

    }

    @Override
    public Search_RecyclerView.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_recycleview_item, parent, false);

        Search_RecyclerView.RecyclerViewHolder recyclerViewHolder = new Search_RecyclerView.RecyclerViewHolder(view);

        return recyclerViewHolder;
    }


    @Override
    public void onBindViewHolder(Search_RecyclerView.RecyclerViewHolder holder, int position) {

        holder.search_word.setText(words.get(position));


    }

    @Override
    public int getItemCount() {
        return words.size();
    }


    public static class RecyclerViewHolder extends RecyclerView.ViewHolder {

        TextView search_word;

        public RecyclerViewHolder(View itemView) {

            super(itemView);
            search_word = (TextView) itemView.findViewById(R.id.search_word);


        }
    }
}
