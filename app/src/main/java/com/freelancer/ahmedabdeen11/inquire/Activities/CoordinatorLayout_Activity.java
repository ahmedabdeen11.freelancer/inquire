package com.freelancer.ahmedabdeen11.inquire.Activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.freelancer.ahmedabdeen11.inquire.Fragments.FragmentA;
import com.freelancer.ahmedabdeen11.inquire.Fragments.Item1;
import com.freelancer.ahmedabdeen11.inquire.Fragments.Item2;
import com.freelancer.ahmedabdeen11.inquire.Fragments.Item3;
import com.freelancer.ahmedabdeen11.inquire.Fragments.Search_Fragment;
import com.freelancer.ahmedabdeen11.inquire.R;

public class CoordinatorLayout_Activity extends AppCompatActivity {

    static int initialHeight;
    ViewPager viewPager;
    TabLayout tabLayout;
    RelativeLayout rl;
    LinearLayout ll;
    Toolbar tb;
    boolean search_status = false;

    //---------------Collapse Animate------------------//
    public static void collapse(final View v) {
        initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration(400);
        v.startAnimation(a);
    }

    //------------Expand Animate-----------//
    public static void expand(final View v) {
        v.measure(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);

        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewPager.LayoutParams.WRAP_CONTENT
                        : (int) (initialHeight * interpolatedTime);

                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration(400);
        v.startAnimation(a);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coordinatorlayout);


        viewPager = (ViewPager) findViewById(R.id.pager2);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout2);

        rl = (RelativeLayout) findViewById(R.id.main_toolbar);
        ll = (LinearLayout) findViewById(R.id.search_layout);
        tb = (Toolbar) findViewById(R.id.mytoolbar);

        viewPager.setAdapter(new CoordinatorLayout_Activity.MyAdaper(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
    }

    public void Search(View view) {
        rl.setVisibility(View.GONE);
        ll.setVisibility(View.VISIBLE);
        tb.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));

        android.app.Fragment searchFragment = new Search_Fragment();
        android.app.FragmentManager fragmentManager = getFragmentManager();
        android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.pager_frame, searchFragment, "SearchFragment");
        fragmentTransaction.commit();

        collapse(tabLayout);

        search_status = true;
    }

    public void cancel_Search(View view) {
        ll.setVisibility(View.GONE);
        rl.setVisibility(View.VISIBLE);
        tb.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        android.app.FragmentManager fragmentManager = getFragmentManager();
        android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.remove(getFragmentManager().findFragmentById(R.id.pager_frame));
        fragmentTransaction.commit();

        expand(tabLayout);

        search_status = false;
    }

    @Override
    public void onBackPressed() {

        if (search_status) {
            ll.setVisibility(View.GONE);
            rl.setVisibility(View.VISIBLE);
            tb.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
            android.app.FragmentManager fragmentManager = getFragmentManager();
            android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(getFragmentManager().findFragmentById(R.id.pager_frame));
            fragmentTransaction.commit();
            expand(tabLayout);
            search_status = false;
        } else {
            super.onBackPressed();
        }
    }

    class MyAdaper extends FragmentPagerAdapter {

        public MyAdaper(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = new Item1();
                    break;
                case 1:
                    fragment = new Item2();
                    break;
                case 2:
                    fragment = new Item3();
                    break;
                default:
                    fragment = new FragmentA();
            }

            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return "ITEM ONE";
            } else if (position == 1) {
                return "ITEM TWO";
            } else if (position == 2) {
                return "ITEM THREE";
            } else {
                return "ITEM";
            }

        }
    }
}
