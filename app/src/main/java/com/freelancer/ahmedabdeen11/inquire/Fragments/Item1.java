package com.freelancer.ahmedabdeen11.inquire.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.freelancer.ahmedabdeen11.inquire.Adapters.Adapter_test_Gridview;
import com.freelancer.ahmedabdeen11.inquire.R;

import java.util.ArrayList;

/**
 * Created by A7MED-Freelancer on 29-May-17.
 */

public class Item1 extends Fragment {

    GridView mygridvew;
    Adapter_test_Gridview adapter;
    ArrayList<Integer> items;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item1, container, false);

        items = new ArrayList<>();
        items.add(R.drawable.img9);
        items.add(R.drawable.img5);
        items.add(R.drawable.img6);
        items.add(R.drawable.img2);
        items.add(R.drawable.img10);
        items.add(R.drawable.img7);

        mygridvew = (GridView) view.findViewById(R.id.mygridview);
        adapter = new Adapter_test_Gridview(getActivity(), items);
        mygridvew.setAdapter(adapter);

        return view;
    }
}
