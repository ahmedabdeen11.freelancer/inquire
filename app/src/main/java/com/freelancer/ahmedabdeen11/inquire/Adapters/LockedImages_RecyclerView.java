package com.freelancer.ahmedabdeen11.inquire.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.freelancer.ahmedabdeen11.inquire.R;

import java.util.ArrayList;


/**
 * Created by A7MED-Freelancer on 25-May-17.
 */

public class LockedImages_RecyclerView extends RecyclerView.Adapter<LockedImages_RecyclerView.RecyclerViewHolder> {

    ArrayList<Integer> imgs;
    Context c;
    int availableImageCount;


    public LockedImages_RecyclerView(Context c, ArrayList<Integer> imgs, int availableImageCount) {
        this.c = c;

        this.imgs = imgs;
        this.availableImageCount = availableImageCount - 1;

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.locked_images_item, parent, false);

        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);

        return recyclerViewHolder;
    }


    @Override
    public void onBindViewHolder(LockedImages_RecyclerView.RecyclerViewHolder holder, int position) {

        Glide.with(c).load(imgs.get(position)).into(holder.img);
        holder.lock.setVisibility(View.GONE);
        holder.blur.setVisibility(View.GONE);

        if (position > this.availableImageCount) {
            holder.lock.setVisibility(View.VISIBLE);
            holder.blur.setVisibility(View.VISIBLE);

            holder.lock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(c);
                    builder.setMessage("Upgrade to the paid version of the app");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    android.app.AlertDialog alertDialog = builder.create();

                    alertDialog.show();

                }
            });


            holder.blur.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(c);

                    builder.setMessage("Upgrade to the paid version of the app");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    android.app.AlertDialog alertDialog = builder.create();

                    alertDialog.show();
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return imgs.size();
    }


    public static class RecyclerViewHolder extends RecyclerView.ViewHolder {

        ImageView img;
        ImageView lock;
        View blur;

        public RecyclerViewHolder(View itemView) {

            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.recycle_image);
            lock = (ImageView) itemView.findViewById(R.id.lock);
            blur = itemView.findViewById(R.id.blur);

        }
    }
}
