package com.freelancer.ahmedabdeen11.inquire.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.freelancer.ahmedabdeen11.inquire.Model.User;
import com.freelancer.ahmedabdeen11.inquire.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class Create_Account_Activity extends AppCompatActivity {

    private static final int REQUEST_TAKE_GALLERY_PHOTO = 1;
    private static final int REQUEST_EXTERNAL_STORAGE = 2;
    // Storage Permissions
    private static String[] PERMISSIONS_STORAGE = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    EditText email;
    EditText pass;
    EditText username;
    String profileImage_Path;
    ImageView profile_image;
    Uri mImageUri;
    StorageReference photoRef;
    Uri downloadUrl;
    User user;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private boolean stopUserInteractions = false;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;
    private FirebaseStorage mFirebaseStorage;
    private StorageReference mUsersImagesReference;

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.freelancer.ahmedabdeen11.inquire.R.layout.activity_create__account_);


        mAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mFirebaseStorage = FirebaseStorage.getInstance();

        mDatabaseReference = mFirebaseDatabase.getReference().child("accounts");
        mUsersImagesReference = mFirebaseStorage.getReference().child("users_images");

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull final FirebaseAuth firebaseAuth) {


            }
        };

        username = (EditText) findViewById(R.id.usernameEntry);
        email = (EditText) findViewById(R.id.emailEntry);
        pass = (EditText) findViewById(R.id.passwordEntry);
        profile_image = (ImageView) findViewById(R.id.profileImage);


    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.addAuthStateListener(mAuthListener);
        }
    }

    public void createacc(View view) {

        if (!validateForm()) {
            Toast.makeText(this, "Please Complete The Required Fields", Toast.LENGTH_SHORT).show();
        } else {

            if (mImageUri == null) {
                Toast.makeText(Create_Account_Activity.this, "Please Choose Image", Toast.LENGTH_SHORT).show();
            } else {

                String tx_email = email.getText().toString();
                String tx_pass = pass.getText().toString();

                stopUserInteractions = true;
                mAuth.createUserWithEmailAndPassword(tx_email, tx_pass)
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (!task.isSuccessful()) {

                                    Toast.makeText(Create_Account_Activity.this, "Authentication Failed", Toast.LENGTH_SHORT).show();
                                    stopUserInteractions = false;

                                } else {

                                    // Signup Success
                                    if (mAuth.getCurrentUser() != null) {


                                        photoRef.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                            @Override
                                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                                downloadUrl = taskSnapshot.getDownloadUrl();
                                                String img_name = taskSnapshot.getStorage().getName();

                                                user = new User(mAuth.getCurrentUser().getUid(), username.getText().toString(), email.getText().toString(), downloadUrl.toString());
                                                mDatabaseReference.push().setValue(user);
                                                Intent intent = new Intent(getApplicationContext(), Custom_List_Activity.class);
                                                startActivity(intent);
                                                finish();
                                            }
                                        });


                                        stopUserInteractions = false;
                                    }

                                }

                            }
                        });
            }

        }
    }

    private boolean validateForm() {
        boolean valid = true;
        String mEmail = email.getText().toString();
        if (TextUtils.isEmpty(mEmail)) {
            email.setError("Required");
            valid = false;
        } else {
            email.setError(null);
        }

        String mPass = pass.getText().toString();
        if (TextUtils.isEmpty(mPass)) {
            pass.setError("Required");
            valid = false;
        } else {
            pass.setError(null);
        }


        String mUserName = username.getText().toString();
        if (TextUtils.isEmpty(mUserName)) {
            username.setError("Required");
            valid = false;
        } else {
            username.setError(null);
        }


        return valid;
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (stopUserInteractions) {
            return true;
        } else {
            return super.dispatchTouchEvent(ev);
        }
    }

    public void pickImage(View view) {
        Intent intent = new Intent();
        intent.setType("image/*");
//        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_TAKE_GALLERY_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            //-------------Pick Image -------//

            // When an Image is picked
            if (requestCode == REQUEST_TAKE_GALLERY_PHOTO && resultCode == RESULT_OK
                    && null != data) {

                verifyStoragePermissions(this);
                // Get the Image from data

                Uri selectedVideoUri = data.getData();

                if (data.getData() != null) {

                    mImageUri = data.getData();

//                    profileImage_Path = get_ImagePath(mImageUri);

                    profile_image.setImageURI(mImageUri);
                    photoRef = mUsersImagesReference.child(mImageUri.getLastPathSegment());
                }
            }
        }
    }
}
