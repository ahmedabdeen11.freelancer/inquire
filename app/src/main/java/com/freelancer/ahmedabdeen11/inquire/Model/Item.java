package com.freelancer.ahmedabdeen11.inquire.Model;

/**
 * Created by A7MED-Freelancer on 14-May-17.
 */

public class Item {

    String key;
    String value;

    public Item() {
    }

    public Item(String key, String value) {

        this.key = key;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
