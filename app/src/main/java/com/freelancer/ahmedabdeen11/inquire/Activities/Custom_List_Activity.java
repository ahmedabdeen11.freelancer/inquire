package com.freelancer.ahmedabdeen11.inquire.Activities;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.freelancer.ahmedabdeen11.inquire.Adapters.Custom_List_Adapter;
import com.freelancer.ahmedabdeen11.inquire.Model.Item;
import com.freelancer.ahmedabdeen11.inquire.R;
import com.freelancer.ahmedabdeen11.inquire.Services.Datachange_Service;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Custom_List_Activity extends AppCompatActivity {

    ListView custom_listview;
    Custom_List_Adapter adapter;
    ArrayList<Item> all_items;
    RadioButton enable_radio;
    RadioButton disable_radio;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;
    private ChildEventListener mChildEventListener;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom__list);

        all_items = new ArrayList<>();
        custom_listview = (ListView) findViewById(R.id.custom_listview);
        adapter = new Custom_List_Adapter(getApplicationContext(), all_items);
        custom_listview.setAdapter(adapter);

        mAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference().child("custom_list");

        mDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    // TODO: handle the case where the data already exists

                } else {
                    // TODO: handle the case where the data does not yet exist
                    Toast.makeText(Custom_List_Activity.this, "No Data Found", Toast.LENGTH_SHORT).show();

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Log.d("trace", "outside");
//        mDatabaseReference.child("-KaHpYzJMjCRGSjeShYz").removeValue();

        mChildEventListener = new ChildEventListener() {


            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {


                Object obj = dataSnapshot.getValue();
                String myitem = obj.toString();

                Object obj2 = dataSnapshot.getKey();
                String mykey = obj2.toString();

                all_items.add(new Item(mykey, myitem));
                adapter.notifyDataSetChanged();


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                Object obj = dataSnapshot.getKey();
                String mykey = obj.toString();

                Object obj2 = dataSnapshot.getValue();
                String myvalue = obj2.toString();

                for (int i = 0; i < all_items.size(); i++) {
                    if (all_items.get(i).getKey().equals(mykey)) {
                        all_items.get(i).setValue(myvalue);
                        break;
                    }
                }

                adapter.notifyDataSetChanged();

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Object obj = dataSnapshot.getKey();
                String mykey = obj.toString();

                for (int i = 0; i < all_items.size(); i++) {
                    if (all_items.get(i).getKey().equals(mykey)) {
                        all_items.remove(i);
                        break;
                    }
                }

                adapter.notifyDataSetChanged();

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        mDatabaseReference.addChildEventListener(mChildEventListener);


        //-------------Call The Service ----------//
        enable_radio = (RadioButton) findViewById(R.id.enabled);
        disable_radio = (RadioButton) findViewById(R.id.disapled);

        SharedPreferences prefs = getSharedPreferences("Notification_Status", MODE_PRIVATE);
        String restoredText = prefs.getString("status", null);

        if (restoredText != null) {

            if (restoredText.equals("enabled")) {
                enable_radio.setChecked(true);
                callService();
            } else {
                disable_radio.setChecked(true);
            }


        } else {
            enable_radio.setChecked(true);
            callService();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {

            stopService(new Intent(getApplicationContext(), Datachange_Service.class));
            mAuth.signOut();
            LoginManager.getInstance().logOut();
            Intent intent = new Intent(getApplicationContext(), Login_Activity.class);
            startActivity(intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    public void callService() {
        //----------Service To Check dataChange on Firebase ---------//
        if (isMyServiceRunning(Datachange_Service.class)) {

            //Service is running

        } else {
            Intent intent_service = new Intent(this, Datachange_Service.class);
            startService(intent_service);
        }
    }

    public void Enable_Notification(View view) {

        if (isMyServiceRunning(Datachange_Service.class)) {

        } else {
            Intent intent_service = new Intent(this, Datachange_Service.class);
            startService(intent_service);
            Toast.makeText(this, "Notification Enabled", Toast.LENGTH_SHORT).show();

            SharedPreferences.Editor editor = getSharedPreferences("Notification_Status", MODE_PRIVATE).edit();
            editor.putString("status", "enabled");
            editor.commit();
        }

    }

    public void Disable_Notification(View view) {

        if (isMyServiceRunning(Datachange_Service.class)) {

            Intent intent_service = new Intent(this, Datachange_Service.class);
            stopService(intent_service);

            Toast.makeText(this, "Notification Disabled", Toast.LENGTH_SHORT).show();

            SharedPreferences.Editor editor = getSharedPreferences("Notification_Status", MODE_PRIVATE).edit();
            editor.putString("status", "disabled");
            editor.commit();

        } else {

        }

    }


    @Override
    public void onBackPressed() {
        finish();
    }

}
