package com.freelancer.ahmedabdeen11.inquire.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.freelancer.ahmedabdeen11.inquire.R;

public class Shrinking_ProgressBar extends AppCompatActivity {

    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shrinking__progress_bar);

        progressBar = (ProgressBar) findViewById(R.id.shrink_progressbar);
        progressBar.setProgress(100);
    }


    public void shrink(View view) {

        ProgressBarAnimation anim = new ProgressBarAnimation(progressBar, 100, 0);
        anim.setDuration(1000);
        progressBar.startAnimation(anim);
        
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                
            }

            @Override
            public void onAnimationEnd(Animation animation) {

                //--------Event When Animation End ------//
                Toast.makeText(Shrinking_ProgressBar.this, "Animation Ended", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


    }


    public class ProgressBarAnimation extends Animation {
        private ProgressBar progressBar;
        private float from;
        private float  to;

        public ProgressBarAnimation(ProgressBar progressBar, float from, float to) {
            super();
            this.progressBar = progressBar;
            this.from = from;
            this.to = to;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
            float value = from + (to - from) * interpolatedTime;
            progressBar.setProgress((int) value);
        }

    }

}
