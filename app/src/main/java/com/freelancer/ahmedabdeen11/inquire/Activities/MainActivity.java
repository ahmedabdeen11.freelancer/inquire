package com.freelancer.ahmedabdeen11.inquire.Activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.freelancer.ahmedabdeen11.inquire.Model.User;
import com.freelancer.ahmedabdeen11.inquire.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {

    ImageView profile_image;
    TextView username;
    TextView email;
    TextView id;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;
    private ChildEventListener mChildEventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();


                if (firebaseUser != null) {
                    mDatabaseReference = mFirebaseDatabase.getReference().child("accounts");

                    mChildEventListener = new ChildEventListener() {


                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                            User user = dataSnapshot.getValue(User.class);
                            if (user.getId().equals(mAuth.getCurrentUser().getUid())) {
                                username.setText(user.getName());
                                email.setText(user.getEmail());
                                id.setText(user.getId());

                                Picasso.with(getApplicationContext())
                                        .load(user.getPhotourl())
                                        .fit()
                                        .into(profile_image);
                            }


                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                                Toast.makeText(MainActivity.this, "Child Changed", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

//                                Toast.makeText(MainActivity.this, "Child Removed", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//                                Toast.makeText(MainActivity.this, "Child Moved", Toast.LENGTH_SHORT).show();

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    };

                    mDatabaseReference.addChildEventListener(mChildEventListener);
                }

            }
        };


        username = (TextView) findViewById(R.id.username_view);
        email = (TextView) findViewById(R.id.email_view);
        id = (TextView) findViewById(R.id.id_view);
        profile_image = (ImageView) findViewById(R.id.profileImage);


    }

    @Override
    public void onBackPressed() {

        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }
}
