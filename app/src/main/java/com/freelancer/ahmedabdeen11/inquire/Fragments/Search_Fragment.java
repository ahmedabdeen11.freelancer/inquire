package com.freelancer.ahmedabdeen11.inquire.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.freelancer.ahmedabdeen11.inquire.Adapters.Search_RecyclerView;
import com.freelancer.ahmedabdeen11.inquire.R;

import java.util.ArrayList;

/**
 * Created by A7MED-Freelancer on 29-May-17.
 */

public class Search_Fragment extends Fragment {

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<String> search_words;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_fragment_layout, container, false);

        //------------ Adding Images data----------//

        search_words = new ArrayList<>();
        search_words.add("Java");
        search_words.add("Android Development");
        search_words.add("PHP");
        search_words.add("C#");
        search_words.add("C++");
        search_words.add("MySQL Database");

        recyclerView = (RecyclerView) view.findViewById(R.id.search_recyclerview);
        adapter = new Search_RecyclerView(getActivity(), search_words);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);


        return view;
    }
}
