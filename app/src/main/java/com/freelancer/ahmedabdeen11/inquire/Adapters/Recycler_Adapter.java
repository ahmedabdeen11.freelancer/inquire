package com.freelancer.ahmedabdeen11.inquire.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.freelancer.ahmedabdeen11.inquire.Model.Contact;
import com.freelancer.ahmedabdeen11.inquire.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by A7MED-Freelancer on 20-May-17.
 */

public class Recycler_Adapter extends RecyclerView.Adapter<Recycler_Adapter.RecyclerViewHolder> {

    ArrayList<Contact> contacts;
    Context c;
    int num_of_imgs;

    public Recycler_Adapter(Context c, ArrayList<Contact> contacts, int num_of_imgs) {
        this.c = c;

        this.contacts = new ArrayList<>();
        this.num_of_imgs = num_of_imgs;

        for (int i = 0; i < contacts.size(); i++) {
            this.contacts.add(contacts.get(i));
        }

        if (contacts.size() > num_of_imgs) {

            for (int i = num_of_imgs; i < contacts.size(); i++) {
                this.contacts.remove(num_of_imgs);
            }
        }

        Collections.reverse(this.contacts);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bubble_item_layout, parent, false);

        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);

        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {


        if (position < num_of_imgs) {
            Random rnd = new Random();
            int currentStrokeColor = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

            GradientDrawable bgShape = (GradientDrawable) holder.color.getBackground();
            bgShape.setColor(currentStrokeColor);

            char ch = contacts.get(position).getName().charAt(0);
            ch = Character.toUpperCase(ch);

            holder.letter.setText(String.valueOf(ch));

            if (contacts.get(position).getImage() != 0) {
                holder.img.setImageDrawable(ContextCompat.getDrawable(c, contacts.get(position).getImage()));
            }
        }


    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }


    public static class RecyclerViewHolder extends RecyclerView.ViewHolder {

        ImageView img, color;
        TextView letter;

        public RecyclerViewHolder(View itemView) {

            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.contact_img);
            color = (ImageView) itemView.findViewById(R.id.contact_color);
            letter = (TextView) itemView.findViewById(R.id.contact_first_letter);

        }
    }
}
