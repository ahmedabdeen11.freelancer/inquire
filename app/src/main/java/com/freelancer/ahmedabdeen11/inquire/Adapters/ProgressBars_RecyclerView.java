package com.freelancer.ahmedabdeen11.inquire.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.freelancer.ahmedabdeen11.inquire.R;

import java.util.ArrayList;

/**
 * Created by A7MED-Freelancer on 28-May-17.
 */

public class ProgressBars_RecyclerView extends RecyclerView.Adapter<ProgressBars_RecyclerView.RecyclerViewHolder> {

    ArrayList<Integer[]> progress_list;
    Context c;
    int j = 1;

    public ProgressBars_RecyclerView(Context c, ArrayList<Integer[]> progress_list) {
        this.c = c;
        this.progress_list = progress_list;
    }

    @Override
    public ProgressBars_RecyclerView.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.progressbar_recycleview_item, parent, false);

        ProgressBars_RecyclerView.RecyclerViewHolder recyclerViewHolder = new ProgressBars_RecyclerView.RecyclerViewHolder(view);

        return recyclerViewHolder;
    }


    @Override
    public void onBindViewHolder(ProgressBars_RecyclerView.RecyclerViewHolder holder, int position) {

        holder.title.setText("Recyclerview item" + j);
        ProgressBar_Listview_Adapter adapter = new ProgressBar_Listview_Adapter(c, this.progress_list.get(position));
        holder.mylv.setAdapter(adapter);

        j++;

    }

    @Override
    public int getItemCount() {
        return progress_list.size();
    }


    public static class RecyclerViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        ListView mylv;

        public RecyclerViewHolder(View itemView) {

            super(itemView);

            mylv = (ListView) itemView.findViewById(R.id.progress_listview);
            title = (TextView) itemView.findViewById(R.id.recycleitem_title);

        }
    }
}
