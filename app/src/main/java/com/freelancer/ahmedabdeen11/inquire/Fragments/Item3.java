package com.freelancer.ahmedabdeen11.inquire.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.freelancer.ahmedabdeen11.inquire.R;

/**
 * Created by A7MED-Freelancer on 29-May-17.
 */

public class Item3 extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item3, container, false);

        return view;
    }
}
