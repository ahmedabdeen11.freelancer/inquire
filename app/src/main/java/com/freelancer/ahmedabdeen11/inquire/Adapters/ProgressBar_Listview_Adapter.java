package com.freelancer.ahmedabdeen11.inquire.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.freelancer.ahmedabdeen11.inquire.R;

/**
 * Created by A7MED-Freelancer on 30-May-17.
 */

public class ProgressBar_Listview_Adapter extends ArrayAdapter<Integer> {
    Context c;

    Integer[] obj;

    public ProgressBar_Listview_Adapter(Context c, Integer[] items) {
        super(c, R.layout.progressbar_listview_item, R.id.progress_br, items);
        this.c = c;
        this.obj = items;
    }

    @Override
    public int getCount() {
        return this.obj.length - 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        ProgressBar_Listview_Adapter.MyViewHolder myViewHolder = null;

        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.progressbar_listview_item, parent, false);

            myViewHolder = new ProgressBar_Listview_Adapter.MyViewHolder(row);
            row.setTag(myViewHolder);

        } else {
            myViewHolder = (ProgressBar_Listview_Adapter.MyViewHolder) row.getTag();
        }


        if (this.obj[position] != null) {
            int x = this.obj[position];
            myViewHolder.pb.setProgress(x);
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);
            p.weight = x;
            myViewHolder.progress_txt.setLayoutParams(p);
            myViewHolder.progress_txt.setText(x + "%");
        } else {
            Toast.makeText(c, "Invalid", Toast.LENGTH_SHORT).show();
        }

        return row;
    }

    class MyViewHolder {

        ProgressBar pb;
        TextView progress_txt;

        MyViewHolder(View v) {
            pb = (ProgressBar) v.findViewById(R.id.progress_br);
            progress_txt = (TextView) v.findViewById(R.id.progress_txt);
        }
    }
}
